﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(honirystore.com.Startup))]
namespace honirystore.com
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
